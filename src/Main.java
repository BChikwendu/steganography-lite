import java.awt.*;
import java.io.*;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.util.ArrayList;
import java.util.Scanner;
import javax.imageio.ImageIO;


public class Main {
    public static void encodeBytesInImage(byte[] data, int startIndex, BufferedImage image) {
        int length = data.length;
        assert (length + startIndex <= image.getWidth() * image.getHeight());

        int j = (int) Math.floor((double) startIndex / (double) image.getWidth());
        int x = startIndex;
        for (int i = startIndex; i < startIndex + length; i++) {
            if (x > image.getWidth() - 1) {
                j++;
                x = 0;
            }

            int y = j;


            //store byte i, at position x,y
            int byteToStore = data[i - startIndex];
            int originalRGB = image.getRGB(x, y);
            int newRGB = encodeByteInPixel(byteToStore, originalRGB);
            image.setRGB(x, y, newRGB);

            //System.out.printf("B: %s ,x:%d , y:%d \n" , Integer.toHexString(newRGB),x,y);

            x++;
        }
    }

    public static int encodeByteInPixel(int data, int originalRGB) {
        assert (data < 4096);
        //Data is stored as follows : 1111 | 2222 | . Where 1 = most significant bits => encode in green
        int originalRed = (originalRGB >> 16) & 0xFF;
        int originalGreen = (originalRGB >> 8) & 0xFF;
        int originalBlue = originalRGB & 0xFF;

        int redSectionOfData = (data >> 8) & 0x0F;
        int greenSectionOfData = (data >> 4) & 0x0F;
        int blueSectionOfData = (data & 0x0F);

        //int newRed = (originalRed & 0xF0) + (redSectionOfData);
        int newGreen = (originalGreen & 0xF0) + (greenSectionOfData);
        int newBlue = (originalBlue & 0xF0) + (blueSectionOfData);

        Color color = new Color(originalRed, newGreen, newBlue);
        //int newRGB = ((newRed&0x0ff)<<16)|((newGreen&0x0ff)<<8)|(newBlue&0x0ff);
        return color.getRGB();
    }

    public static Byte decodeByteFromPixel(int pixelRGB) {
        //Data is stored : aaaaaaaa | rrrrrrrr | ggggdddd | bbbbdddd
        // a,r,g,b is colour data. d is our own data. green's data is most significant.

        int greenData = (pixelRGB >> 8) & 0x0F;
        int blueData = pixelRGB & 0x0F;

        int finalData = (greenData << 4) + blueData;
        Integer returnData = finalData;

        return returnData.byteValue();
    }

    public static BufferedImage convertImageToARGB(BufferedImage image) {

        BufferedImage newImage = new BufferedImage(
                image.getWidth(), image.getHeight(),
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = newImage.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return newImage;
    }

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.println("Select a function: 1) Encode file in image\n                   2) Decode file from image");
        int selection = userInput.nextInt();

        if (selection == 1) {
            BufferedImage image = null;

            File inputImageFile;
            File outputFile = new File("C:\\Users\\Brych\\Desktop\\outputimage.png");


            System.out.printf("Select a PNG image file > ");

            String filePath = userInput.next();

            try {
                inputImageFile = new File(filePath);
                image = ImageIO.read(inputImageFile);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }

            if (image != null) {
                image = convertImageToARGB(image);
            }

            int imageWidth = image.getWidth();
            int imageHeight = image.getHeight();

            int maxByteCapacity = imageWidth * imageHeight;

            System.out.println("Maximum size of file to be stored: " + maxByteCapacity + " bytes.");
            System.out.printf("Select a file to encode>  ");

            String inputDataPath = userInput.next();
            File inputDataFile = new File(inputDataPath);
            byte[] data = null;
            try {
                data = Files.readAllBytes(inputDataFile.toPath());
            } catch (java.io.IOException e) {
                e.printStackTrace();
                System.exit(1);
            }

            if (data.length > (maxByteCapacity - 4)) {
                System.out.println("Your data file is too big!");
                System.exit(0);
            }

            //Convert length into 4 bytes
            byte[] lengthData = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder()).putInt(data.length).array();

            encodeBytesInImage(lengthData, 0, image);
            encodeBytesInImage(data, 4, image);

            try {
                ImageIO.write(image, "png", outputFile);
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }

        }
        if (selection == 2) {
            BufferedImage image = null;

            File inputImageFile;
            System.out.printf("Select a PNG image file to decode > ");
            String filePath = userInput.next();
            try {
                inputImageFile = new File(filePath);
                image = ImageIO.read(inputImageFile);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }




            //Retrieve first 4 bytes (First 4 pixels) to get the filesize
            int imageWidth = image.getWidth();
            ArrayList<Integer> sizePixels = new ArrayList<Integer>();
            if (imageWidth > 3) {
                sizePixels.add(image.getRGB(0, 0));
                sizePixels.add(image.getRGB(1, 0));
                sizePixels.add(image.getRGB(2, 0));
                sizePixels.add(image.getRGB(3, 0));
            } else {
                int numberOfWrapArounds = 0;
                int y = 0;
                for (int i = 0; i < 4; i++) {
                    int x = i % imageWidth;
                    if (numberOfWrapArounds >= imageWidth) {
                        y++;
                        numberOfWrapArounds = 0;

                    }
                    sizePixels.add(image.getRGB(x, y));


                    numberOfWrapArounds++;
                }
            }

            byte[] fileSizeData = {0, 0, 0, 0};
            for (int i = 0; i < 4; i++) {
                fileSizeData[i] = decodeByteFromPixel(sizePixels.get(i));
            }

            ByteBuffer buffer = ByteBuffer.wrap(fileSizeData).order(ByteOrder.nativeOrder());
            int fileSize = buffer.getInt();
            System.out.printf("Detected an encoded file of size %d bytes!\n", fileSize);
            ArrayList<Byte> fileData = new ArrayList<Byte>();

            int timesWrappedAround = 0;
            int y = -1;
            for (int i = 4; i < fileSize+4; i++) {
                if (timesWrappedAround % imageWidth == 0) {
                    y++;
                    timesWrappedAround = 0;
                }
                int x = i % imageWidth;

                int pixelToBeDecoded = image.getRGB(x, y);

                byte fileByte = decodeByteFromPixel(pixelToBeDecoded);


                fileData.add(fileByte);
                timesWrappedAround++;
            }

            System.out.println("Where should I generate an output file?");
            String outputFilePath = userInput.next();
            File outputFile = new File(outputFilePath);


            Byte[] dataAsArrayObject = fileData.toArray(new Byte[fileData.size()]);
            byte[] dataAsArray = new byte[dataAsArrayObject.length];

            int j=0;
            for(Byte b: dataAsArrayObject)
                dataAsArray[j++] = b.byteValue();

            try  {
                Files.write(outputFile.toPath(),dataAsArray);
            } catch (java.io.IOException e){
                e.printStackTrace();
            }

        }
    }
}
